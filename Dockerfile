FROM python:3
RUN apt-get update && apt-get install -y build-essential

ARG version
RUN pip install langflow=="${version?}"
CMD ["langflow", "--host", "0.0.0.0"]
